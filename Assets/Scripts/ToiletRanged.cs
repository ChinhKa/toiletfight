using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToiletRanged : Toilet
{
    protected Pooling pooling;
    public override void Start()
    {
        base.Start();
        pooling = GetComponent<Pooling>();
    }

    public override void AttackProcessing()
    {
        Invoke("Shoot", 0.5f);
    }

    private void Shoot()
    {
        if(target != null)
        {
            GameObject pool = pooling.GetPool();
            pool.transform.position = transform.position;
            pool.SetActive(true);
            pool.GetComponent<Bullet>().SetTarget(target.transform, ATK);
        }
    }
}

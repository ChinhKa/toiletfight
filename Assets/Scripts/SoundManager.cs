﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public static SoundManager ins;
    public AudioSource audioSource_BGMusic;

    [Space]
    [Header("BACKGROUND MUSIC CLIP:")]
    public AudioClip homeClip;
    public AudioClip fightingClip;
    public AudioClip clickButtonClip;
    public AudioClip upgradeClip;
    public AudioClip levelUpClip;
    public AudioClip winClip;
    public AudioClip loseClip;
    public AudioClip coolDownClip;
    public AudioClip spawnCameraClip;
    public AudioClip readyClip;
    public AudioClip explodeClip;
    public AudioClip collectionCoinClip;
    public AudioClip zombieGrowlingSound;

    [Space]
    [Header("AUDIO POOLING:")]
    public int numberOfAudio;
    public AudioSource audioPre;
    public List<AudioSource> audioSources = new List<AudioSource>();

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        if (!PlayerPrefs.HasKey(GameConstants.BG_MUSIC))
        {
            Prefs.BG_MUSIC = 1;
        }

        if (!PlayerPrefs.HasKey(GameConstants.SOUND_EFFECT))
        {
            Prefs.SOUND_EFFECT = 1;
        }

        PlayBGMusic(homeClip);
        for(int i = 0; i < numberOfAudio; i++)
        {
            AudioSource audioSource = Instantiate(audioPre);
            audioSources.Add(audioSource);
        }
    }

    public void PlayBGMusic(AudioClip clip)
    {
        if (Prefs.BG_MUSIC == 1)
        {
            audioSource_BGMusic.clip = clip;
            audioSource_BGMusic.Play();
        }
    }

    public void PlaySoundEffect(AudioClip clip)
    {
        if (Prefs.SOUND_EFFECT == 1)
        {
            AudioSource audioSource = audioSources.Find(a => a.isPlaying == false);
            if (audioSource != null)
            {
                audioSource.PlayOneShot(clip);
            }
        }
    }

    public void PlaySoundWinLose(AudioClip clip) => audioSources[0].PlayOneShot(clip);
        
    public void AdjustBGSound(bool opt) => audioSource_BGMusic.mute = opt;
    public void AdjustSoundEffect(bool opt)
    {
        foreach (var a in audioSources)
        {
            a.mute = opt;
        }
    }
}

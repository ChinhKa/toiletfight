using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class SoldierCard : MonoBehaviour
{
    public CharacterType type;
    public int rank;
    public TextMeshProUGUI txtLevel;
    public Image icon;
    public Button btnSelect;
    public Image statusIMG;
    public Sprite lockSpr;
    public Sprite activeSprite;

    private void Start()
    {
        btnSelect.onClick.AddListener(() =>
        {
            switch (type)
            {
                case CharacterType.obstacle:
                    Prefs.RANK_SELECTED_OBSTACLE = rank;
                    if(Prefs.CURRENT_RANK_OBSTACLE > rank)
                    {
                        Prefs.LEVEL_SELECTED_OBSTACLE = 3;
                    }
                    else
                    {
                        Prefs.LEVEL_SELECTED_OBSTACLE = Prefs.CURRENT_LEVEL_OBSTACLE;
                    }
                    break;
                case CharacterType.melee:
                    Prefs.RANK_SELECTED_MELEE = rank;
                    if (Prefs.CURRENT_RANK_MELEE > rank)
                    {
                        Prefs.LEVEL_SELECTED_MELEE = 3;
                    }
                    else
                    {
                        Prefs.LEVEL_SELECTED_MELEE = Prefs.CURRENT_LEVEL_MELEE;
                    }
                    break;
                case CharacterType.ranged:
                    Prefs.RANK_SELECTED_RANGED = rank;
                    if (Prefs.CURRENT_RANK_RANGED > rank)
                    {
                        Prefs.LEVEL_SELECTED_RANGED = 3;
                    }
                    else
                    {
                        Prefs.LEVEL_SELECTED_RANGED = Prefs.CURRENT_LEVEL_RANGED;
                    }
                    break;
                default:
                    break;
            }
            SelectedCard card = UIManager.ins.selectedCards.Find(c => c.type == type);
            card.SetInfo();
            UIManager.ins.CheckSoldierCards();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });
    }

    public void SetInfo(CharacterType typeC, int rnk, Sprite sprite)
    {
        type = typeC;
        rank = rnk;
        txtLevel.text = "LV" + rnk;
        icon.sprite = sprite;
    }

    public void SetStatus(Sprite spr)
    {
        statusIMG.sprite = spr;

        if (spr == null)
        {
            statusIMG.gameObject.SetActive(false);
        }
        else
        {
            statusIMG.gameObject.SetActive(true);
        }
    }
}

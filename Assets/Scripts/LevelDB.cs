using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelDB", menuName = "New Level DB")]
public class LevelDB : ScriptableObject
{
    public List<Phase> listPhase = new List<Phase>();
}

[System.Serializable]
public class Phase
{
    public string name;
    public Sprite icon;
    public List<MapInfo> listRound = new List<MapInfo>();   
}

[System.Serializable]
public class MapInfo
{
    public Transform map;
    public int rewardsRound;
    public List<Wave> listWave = new List<Wave>();
}

[System.Serializable]
public class Wave
{
    public List<WaveInfo> toiletListAppears = new List<WaveInfo>();
}

[System.Serializable]
public class WaveInfo
{
    public CharacterType type;
    public int numbersToilet;
    public Transform pre;
}

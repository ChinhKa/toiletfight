using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "BossCharacterDB", menuName = "New Boss Character DB")]
public class BossCharacterDB : ScriptableObject
{
    public List<BossInfo> listCharacter = new List<BossInfo>();
}

[System.Serializable]
public class BossInfo : ToiletInfo
{
    public float gold;
    public float skillUsageTime;
    public float skillDamage;
    public AudioClip skillSound;
}

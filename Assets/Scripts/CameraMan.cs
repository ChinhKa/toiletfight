using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMan : Character
{
    private bool isReady;
    public override void Start()
    {
        base.Start();
        StartCoroutine(Ready());
    }

    public IEnumerator Ready()
    {
        yield return new WaitForSeconds(0.2f);
        isReady = true;
    }

    public override void Move()
    {
        if (isReady)
        {
            if (GameManager.ins.listToilet.Count > 0)
            {
                if (target != null)
                {
                    if (Vector2.Distance(transform.position, target.transform.position) > range)
                    {
                        CheckDir(); 
                        FollowTarget();
                    }
                    else
                    {
                        Attack();
                    }
                }
                else
                {
                    DetermineTheTarget(GameManager.ins.listToilet);
                    allowTriggeringAnim_Run = true;
                }
            }
        }
    }

    public override void CheckDir()
    {
        float xDir;
        if (target.position.x > transform.position.x)
        {
            xDir = 1;
        }
        else
        {
            xDir = -1;
        }
        transform.GetChild(0).localScale = new Vector3(Mathf.Abs(transform.GetChild(0).localScale.x) * xDir, transform.GetChild(0).localScale.y, transform.GetChild(0).localScale.z);
    }

    public virtual void FollowTarget()
    {
        rigidbody.constraints = RigidbodyConstraints2D.None;
        if (allowTriggeringAnim_Run)
        {
            PlayAnimation(AnimationName.Run.ToString(), true);
            allowTriggeringAnim_Run = false;
        }
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, moveSpeed * Time.deltaTime);
    }

    public override void TakeDame(float dmg)
    {
        HP -= dmg;
        if (!isDie)
        {
            if (HP <= 0)
            {
                GameManager.ins.listCamera.Remove(transform);
                PlayAnimation(AnimationName.Die.ToString(), false);
                Destroy(gameObject, 0.5f);
                isDie = true;
                this.enabled = false;
            }
            else
            {
                PlayAnimation(AnimationName.Hit.ToString(), false);
            }
        }
    }
}

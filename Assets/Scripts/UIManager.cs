using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    public static UIManager ins;

    [Space]
    [Header("UI:")]
    public GameObject home_UI;
    public GameObject gamePlay_UI;
    public GameObject waiting_UI;
    public GameObject gameLose_UI;
    public GameObject gameWon_UI;
    public GameObject upgrade_UI;
    public GameObject fight_UI;
    public GameObject settingHome_UI;
    public GameObject settingInGame_UI;
    public GameObject mess_NotEnoughtEnergy_UI;
    public GameObject eventUI;
    public GameObject shopUI;
    public GameObject reward_UI;
    public GameObject Mess_NotEnoughtSoldier_UI;
    public GameObject Mess_NoInternet_UI;

    [Space]
    [Header("BUTTON:")]
    public Button btnSelect_Obstacle;
    public Button btnSelect_Melee;
    public Button btnSelect_Ranged;
    public Button btnPlay;
    public Button btnPlane_Item;
    public Button btnHero_Item;
    public Button btnFight_Page;
    public Button btnUpgrade_Page;
    public Button btnSetting_Page;
    public Button btnBackHome_SettingUI;
    public Button btnBackHome_WinGameUI;
    public Button btnBackHome_GameOverUI;
    public Button btnSetting_InGame;
    public Button btnCloseSettingInGame;
    public Button btnAddEnergy_HomeUI;
    public Button btnAddEnergy_InGame;
    public Button btnEvent_Page;
    public Button btnShop_Page;
    public Button btnNextRound;
    public Button btnNextMap;
    public Button btnBackMap;
    public Button btnClose_Mess_NotEnoughtEnergy_UI;
    public Button btnAddEnergy_MessUI;
    public Button btnReward;
    public Button btnCloseReward_UI;
    public Button btnCloseNotEnoughtSoldier_UI;
    public Button btnAddCoin;
    public Button btnAddRuby;
    public Button btnRefreshInternet;

    [Space]
    [Header("MUSIC SETTING IN HOME UI")]
    public Button btnOnBGMusic_HomeUI;
    public Button btnOffBGMusic_HomeUI;
    public Button btnOnSoundEffect_HomeUI;
    public Button btnOffSoundEffect_HomeUI;
    public Button btnOnVibrate_HomeUI;
    public Button btnOffVibrate_HomeUI;

    [Space]
    [Header("MUSIC SETTING IN GAME UI")]
    public Button btnOnBGMusic_InGame;
    public Button btnOffBGMusic_InGame;
    public Button btnOnSoundEffect_InGame;
    public Button btnOffSoundEffect_InGame;
    public Button btnOnVibrate_InGame;
    public Button btnOffVibrate_InGame;

    public Button btnX2Coin;
    public Button btnRewardX2;

    [Space]
    [Header("SLIDER:")]
    public Slider slider_Energy_InGame;
    public Slider slider_Energy_Home;

    [Space]
    [Header("TEXT:")]
    public TextMeshProUGUI txtEnergy_InGame;
    public TextMeshProUGUI txtEnergy_Home;
    public TextMeshProUGUI txtCountDown_WaitingUI;
    public TextMeshProUGUI txtCoin;
    public TextMeshProUGUI txtRuby;
    public TextMeshProUGUI txtRound_InGame;
    public TextMeshProUGUI txtRound_Home;
    public TextMeshProUGUI txtPhase;
    public TextMeshProUGUI txtEnergyReward;
    public TextMeshProUGUI txtRewardRound;
    public TextMeshProUGUI txtRewardMission_Coin;
    public TextMeshProUGUI txtRewardMission_Ruby;
    public TextMeshProUGUI txtRewardsRoundInfo;

    [Space]
    [Header("TXT CAMERA COST:")]
    public TextMeshProUGUI txtObstacleCost;
    public TextMeshProUGUI txtCameraCost_Melee;
    public TextMeshProUGUI txtCameraCost_Ranged;

    [Space]
    [Header("IMAGE:")]
    public Image IconMap;
    public List<Image> listProgress = new List<Image>();
    public GameObject progressBar;
    public Sprite Selected_CharacterType_IMG;
    public Sprite NotSelected_CharacterType_IMG;

    public Image obstacle_Icon;
    public Image melee_Icon;
    public Image ranged_Icon;

    [Space]
    [Header("CLOUDS:")]
    public Transform cloud_L;
    public Transform cloud_R;

    public List<SelectedCard> selectedCards = new List<SelectedCard>();

    [Space]
    [Header("CARD COLUMN:")]
    public Transform obstacleColumn;
    public Transform meleeColumn;
    public Transform rangedColumn;

    public Transform soldierCard_Pre;
    public Transform energyDrop_Pre;
    public Transform coinDrop_Pre;
    public Transform rubyDrop_Pre;
    public Transform energySlider_Icon;
    public List<SoldierCard> soldierCards = new List<SoldierCard>();
    private int idxMap;
    public Transform canvas;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        idxMap = Prefs.PHASE;
        ClickEvent();
        SetActiveUI();
        ShowEnergyBar_Home();
        ShowCoin();
        ShowRuby();
        Active(fight_UI);
        ShowMapInfo();
        if (Prefs.BG_MUSIC == 0)
        {
            ActiveButtonSetting(btnOnBGMusic_HomeUI, btnOffBGMusic_HomeUI, 0);
            ActiveButtonSetting(btnOnBGMusic_InGame, btnOffBGMusic_InGame, 0);
        }
        else
        {
            ActiveButtonSetting(btnOffBGMusic_HomeUI, btnOnBGMusic_HomeUI, 1);
            ActiveButtonSetting(btnOffBGMusic_InGame, btnOnBGMusic_InGame, 1);
        }

        if (Prefs.SOUND_EFFECT == 0)
        {
            ActiveButtonSetting(btnOnSoundEffect_HomeUI, btnOffSoundEffect_HomeUI, 0);
            ActiveButtonSetting(btnOnSoundEffect_InGame, btnOffSoundEffect_InGame, 0);
        }
        else
        {
            ActiveButtonSetting(btnOffSoundEffect_HomeUI, btnOnSoundEffect_HomeUI, 1);
            ActiveButtonSetting(btnOffSoundEffect_InGame, btnOnSoundEffect_InGame, 1);
        }

        if (Prefs.VIBRATE == 0)
        {
            ActiveButtonSetting(btnOnVibrate_HomeUI, btnOffVibrate_HomeUI, 0);
            ActiveButtonSetting(btnOnVibrate_InGame, btnOffVibrate_InGame, 0);
        }
        else
        {
            ActiveButtonSetting(btnOffVibrate_HomeUI, btnOnVibrate_HomeUI, 1);
            ActiveButtonSetting(btnOffVibrate_InGame, btnOnVibrate_InGame, 1);
        }
        foreach (var o in selectedCards)
        {
            o.SetInfo();
        }
        ActiveUpgradeButtonCard();

        CheckRewardsMission();
        ShowRewardsRound();
        Spawn_SoldierCard(DataManager.ins.data.camera_Obstacle_DB, obstacleColumn);
        Spawn_SoldierCard(DataManager.ins.data.camera_Melee_DB, meleeColumn);
        Spawn_SoldierCard(DataManager.ins.data.camera_Ranged_DB, rangedColumn);
        CheckInternet();
        SwitchTap_Menu(btnFight_Page);
    }

    private void CheckInternet()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Mess_NoInternet_UI.SetActive(true);
        }
        else
        {
            Mess_NoInternet_UI.SetActive(false);
        }
    }

    private void CheckRewardsMission()
    {
        Debug.Log("RS" + Prefs.REWARDS_TIME);
        txtRewardsRoundInfo.text = "To: " + DataManager.ins.data.listRewardsMission[Prefs.REWARDS_TIME].roundName;
        ShowRewardsMission();
      /*  if(Prefs.REWARDS_RECEIVED == 1)
        {
            if (Prefs.REWARDS_TIME < ((Prefs.PHASE + 1) * 2)-1)
            {
                Prefs.REWARDS_RECEIVED = 0;
            }
        }*/
       
        if (Prefs.REWARDS_RECEIVED == 0)
        {
            if (Prefs.ROUND >= 4)
            {
                btnReward.transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                btnReward.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        else
        {
            btnReward.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    private void SetActiveUI()
    {
        home_UI.SetActive(true);
        reward_UI.SetActive(false);
        Mess_NotEnoughtSoldier_UI.SetActive(false);
        mess_NotEnoughtEnergy_UI.SetActive(false);
        Mess_NoInternet_UI.gameObject.SetActive(false);
    }

    public void ShowCoin() => txtCoin.text = Prefs.COIN.ToString();
    public void ShowRuby() => txtRuby.text = Prefs.RUBY.ToString();
    public void ShowRound() => txtRound_InGame.text = txtRound_Home.text = "Round: " + (Prefs.PHASE + 1) + "-" + (Prefs.ROUND + 1);
    public void ShowRewardsRound()
    {
        MapInfo info = DataManager.ins.data.levelDB.listPhase[Prefs.PHASE].listRound[Prefs.ROUND];
        txtRewardRound.text = info.rewardsRound.ToString();
    }
    public void ShowRewardsMission()
    {
        txtRewardMission_Coin.text = DataManager.ins.data.listRewardsMission[Prefs.REWARDS_TIME].rewardsMission_Coin.ToString();
        txtRewardMission_Ruby.text = DataManager.ins.data.listRewardsMission[Prefs.REWARDS_TIME].rewardsMission_Ruby.ToString();
    }

    public void ShowEnergyBar_Home()
    {
        if (Prefs.ENERGY <= 0)
        {
            txtEnergy_Home.text = "";
        }
        else
        {
            mess_NotEnoughtEnergy_UI.SetActive(false);
            txtEnergy_Home.text = Prefs.ENERGY + "/5";
        }
        slider_Energy_Home.maxValue = 5;
        slider_Energy_Home.value = Prefs.ENERGY;
    }

    public void Spawn_SoldierCard(CameraCharacterDB db, Transform column)
    {
        foreach (var o in db.listCharacter)
        {
            Transform pre = Instantiate(soldierCard_Pre);
            pre.SetParent(column);
            pre.GetComponent<SoldierCard>().SetInfo(o.type, (int)o.rank, o.icon);
            soldierCards.Add(pre.GetComponent<SoldierCard>());

        }
        CheckSoldierCards();
    }

    public void CheckSoldierCards()
    {
        foreach (var c in soldierCards)
        {
            switch (c.type)
            {
                case CharacterType.obstacle:
                    SetInteract_SoldierCardsButton(GameConstants.CURRENT_RANK_OBSTACLE, GameConstants.RANK_SELECTED_OBSTACLE, c);
                    break;
                case CharacterType.melee:
                    SetInteract_SoldierCardsButton(GameConstants.CURRENT_RANK_MELEE, GameConstants.RANK_SELECTED_MELEE, c);
                    break;
                case CharacterType.ranged:
                    SetInteract_SoldierCardsButton(GameConstants.CURRENT_RANK_RANGED, GameConstants.RANK_SELECTED_RANGED, c);
                    break;
                default:
                    break;
            }
        }
    }

    public void SetInteract_SoldierCardsButton(string rankCurrent, string rankSelected, SoldierCard soldierCard)
    {
        int rankC = PlayerPrefs.GetInt(rankCurrent);
        int rankS = PlayerPrefs.GetInt(rankSelected);
        if (soldierCard.rank <= rankC)
        {
            if (soldierCard.rank == rankS)
            {
                soldierCard.SetStatus(soldierCard.activeSprite);
            }
            else
            {
                soldierCard.SetStatus(null);
            }
            soldierCard.btnSelect.interactable = true;
        }
        else
        {
            soldierCard.btnSelect.interactable = false;
            soldierCard.SetStatus(soldierCard.lockSpr);
        }
    }

    public bool CheckEmpty_SoldierCard()
    {
        foreach (var c in selectedCards)
        {
            if (c.empty)
            {
                Mess_NotEnoughtSoldier_UI.SetActive(true);
                return true;
            }
        }
        return false;
    }

    public void ActiveUpgradeButtonCard()
    {
        foreach (var c in selectedCards)
        {
            if (Prefs.COIN >= c.upgradeCost && !c.isMax)
            {
                c.btnUpgrade.interactable = true;
            }
            else
            {
                c.btnUpgrade.interactable = false;
            }
        }
    }

    private void ClickEvent()
    {
        btnSelect_Obstacle.onClick.AddListener(() =>
        {
            GameManager.ins.SetSpawnInfo_Camera(CharacterType.obstacle);
            Selected_Character_Type(btnSelect_Obstacle);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnSelect_Melee.onClick.AddListener(() =>
        {
            GameManager.ins.SetSpawnInfo_Camera(CharacterType.melee);
            Selected_Character_Type(btnSelect_Melee);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnSelect_Ranged.onClick.AddListener(() =>
        {
            GameManager.ins.SetSpawnInfo_Camera(CharacterType.ranged);
            Selected_Character_Type(btnSelect_Ranged);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnAddEnergy_HomeUI.onClick.AddListener(() =>
        {
            AddEnergy_Home();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnAddEnergy_MessUI.onClick.AddListener(() =>
        {
            AddEnergy_Home();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnPlay.onClick.AddListener(() =>
        {
            if (Prefs.ENERGY > 0)
            {
                Prefs.ENERGY--;
                Active(null);
                MapProcessing(false);
                SoundManager.ins.PlayBGMusic(SoundManager.ins.fightingClip);
            }
            else
            {
                mess_NotEnoughtEnergy_UI.SetActive(true);
            }
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnPlane_Item.onClick.AddListener(() =>
        {
            GameManager.ins.UsePlaneItem();
            btnPlane_Item.interactable = false;
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnHero_Item.onClick.AddListener(() =>
        {
            GameManager.ins.SpawnHero();
            btnHero_Item.interactable = false;
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnNextRound.onClick.AddListener(() =>
        {
            gameWon_UI.SetActive(false);
            MapProcessing(true);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnUpgrade_Page.onClick.AddListener(() =>
        {
            Active(upgrade_UI);
            SwitchTap_Menu(btnUpgrade_Page);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnFight_Page.onClick.AddListener(() =>
        {
            if (!CheckEmpty_SoldierCard())
            {
                Active(fight_UI);
                SwitchTap_Menu(btnFight_Page);
            }
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnSetting_Page.onClick.AddListener(() =>
        {
            if (!CheckEmpty_SoldierCard())
            {
                Active(settingHome_UI);
                SwitchTap_Menu(btnSetting_Page);
            }
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnSetting_InGame.onClick.AddListener(() =>
        {
            Time.timeScale = 0;
            Active(settingInGame_UI);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnBackHome_SettingUI.onClick.AddListener(() =>
        {
            Time.timeScale = 1;
            GameManager.ins.ReloadScene();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnBackHome_GameOverUI.onClick.AddListener(() =>
        {
            GameManager.ins.ReloadScene();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnBackHome_WinGameUI.onClick.AddListener(() =>
        {
            GameManager.ins.ReloadScene();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnEvent_Page.onClick.AddListener(() =>
        {
            if (!CheckEmpty_SoldierCard())
            {
                Active(eventUI);
                SwitchTap_Menu(btnEvent_Page);
            }
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnAddCoin.onClick.AddListener(() =>
        {
            Prefs.COIN += DataManager.ins.data.adsCoinReward;
            ActiveUpgradeButtonCard();
            ShowCoin();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnAddRuby.onClick.AddListener(() =>
        {
            Prefs.RUBY += DataManager.ins.data.adsRubyReward;
            ShowRuby();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnShop_Page.onClick.AddListener(() =>
        {
            if (!CheckEmpty_SoldierCard())
            {
                Active(shopUI);
                SwitchTap_Menu(btnShop_Page);
            }
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnClose_Mess_NotEnoughtEnergy_UI.onClick.AddListener(() =>
        {
            mess_NotEnoughtEnergy_UI.SetActive(false);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnCloseNotEnoughtSoldier_UI.onClick.AddListener(() =>
        {
            Mess_NotEnoughtSoldier_UI.SetActive(false);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnCloseSettingInGame.onClick.AddListener(() =>
        {
            Time.timeScale = 1;
            Active(gamePlay_UI);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnRefreshInternet.onClick.AddListener(() =>
        {
            CheckInternet();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnAddEnergy_InGame.onClick.AddListener(() =>
        {
            for (int i = 0; i < DataManager.ins.data.energyReward; i++)
            {
                Vector3 random = new Vector3(Random.Range(btnAddEnergy_InGame.transform.position.x - 200, btnAddEnergy_InGame.transform.position.x + 200)
                    , Random.Range(btnAddEnergy_InGame.transform.position.y - 200, btnAddEnergy_InGame.transform.position.y + 200), 0);
                Transform pre = Instantiate(energyDrop_Pre, random, Quaternion.identity);
                pre.SetParent(canvas);

                pre.DOMove(energySlider_Icon.position, 1f).OnComplete(() =>
                {
                    UpdateSliderEnergy_InGame(-1);
                    Destroy(pre.gameObject);
                });
            }

            btnAddEnergy_InGame.gameObject.SetActive(false);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.collectionCoinClip);
        });

        btnNextMap.onClick.AddListener(() =>
        {
            idxMap++;
            ShowMapInfo();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnBackMap.onClick.AddListener(() =>
        {
            idxMap--;
            ShowMapInfo();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnClose_Mess_NotEnoughtEnergy_UI.onClick.AddListener(() =>
        {
            mess_NotEnoughtEnergy_UI.SetActive(false);
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnReward.onClick.AddListener(() =>
        {
            if (btnReward.transform.GetChild(0).gameObject.activeInHierarchy)
            {
                reward_UI.SetActive(true);
                btnReward.transform.GetChild(0).gameObject.SetActive(false);
            }

            CheckRewardsMission();

            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnCloseReward_UI.onClick.AddListener(() =>
        {
            GameManager.ins.ReceiveRewardsMission();
            ShowCoin();
            ShowRuby();
            reward_UI.SetActive(false);
            RewardEffect(btnCloseReward_UI, 5, coinDrop_Pre, txtCoin.transform);
            RewardEffect(btnCloseReward_UI, 5, rubyDrop_Pre, txtRuby.transform);
            Prefs.REWARDS_RECEIVED = 1;
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
            CheckRewardsMission();
        });

        btnOnBGMusic_HomeUI.onClick.AddListener(() =>
        {
            OnBGMusic();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOnBGMusic_InGame.onClick.AddListener(() =>
        {
            OnBGMusic();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOffBGMusic_HomeUI.onClick.AddListener(() =>
        {
            OffBGMusic();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOffBGMusic_InGame.onClick.AddListener(() =>
        {
            OffBGMusic();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOnSoundEffect_HomeUI.onClick.AddListener(() =>
        {
            OnSoundEffect();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOnSoundEffect_InGame.onClick.AddListener(() =>
        {
            OnSoundEffect();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOffSoundEffect_HomeUI.onClick.AddListener(() =>
        {
            OffSoundEffect();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOffSoundEffect_InGame.onClick.AddListener(() =>
        {
            OffSoundEffect();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOnVibrate_HomeUI.onClick.AddListener(() =>
        {
            OnVibrate();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOnVibrate_InGame.onClick.AddListener(() =>
        {
            OnVibrate();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOffVibrate_HomeUI.onClick.AddListener(() =>
        {
            OffVibrate();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnOffVibrate_InGame.onClick.AddListener(() =>
        {
            OffVibrate();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });

        btnX2Coin.onClick.AddListener(() =>
        {
            GameManager.ins.ReceiveRewardsRound();
            btnX2Coin.gameObject.SetActive(false);
            StartCoroutine(CoinUpEffect());
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.collectionCoinClip);
        });

        btnRewardX2.onClick.AddListener(() =>
        {
            GameManager.ins.ReceiveRewardsMission();
            ShowCoin();
            ShowRuby();
            reward_UI.SetActive(false);
            RewardEffect(btnCloseReward_UI, 5, coinDrop_Pre, txtCoin.transform);
            RewardEffect(btnCloseReward_UI, 5, rubyDrop_Pre, txtRuby.transform);
            Prefs.REWARDS_RECEIVED = 1;
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.collectionCoinClip);
            CheckRewardsMission();
        });
    }

    private void RewardEffect(Button btn, int sl, Transform preEff, Transform target)
    {
        for (int i = 0; i < sl; i++)
        {
            Vector3 random = new Vector3(Random.Range(btn.transform.position.x - 200, btn.transform.position.x + 200)
                , Random.Range(btn.transform.position.y - 200, btn.transform.position.y + 200), 0);
            Transform pre = Instantiate(preEff, random, Quaternion.identity);
            pre.SetParent(canvas);

            pre.DOMove(target.position, 1f).OnComplete(() =>
            {
                Destroy(pre.gameObject);
            });
        }
    }

    private IEnumerator CoinUpEffect()
    {

        int count = 200;
        int nbr = int.Parse(txtRewardRound.text);
        while (count < nbr)
        {
            txtRewardRound.text = (nbr + count).ToString();
            count += 200;
            yield return new WaitForSeconds(0.01f);
        }
        if (count >= nbr)
        {
            txtRewardRound.text = (nbr * 2).ToString();
        }
    }

    private void MapProcessing(bool isComplete)
    {
        if (isComplete)
        {
            if (Prefs.ROUND >= DataManager.ins.data.levelDB.listPhase[Prefs.PHASE].listRound.Count - 1)
            {
                Prefs.PHASE++;
                Prefs.ROUND = 0;
            }
            else
            {
                Prefs.ROUND++;
            }
        }

        btnPlane_Item.interactable = true;
        btnHero_Item.interactable = true;
        SetUpEnergyReward();
        Selected_Character_Type(null);
        ShowRound();
        SetMax_SliderEnergy_InGame(100);
        CloseTheCloud();
    }

    Tween tw1, tw2;
    private void SetUpEnergyReward()
    {
        tw1.Kill();
        tw2.Kill();
        txtEnergyReward.text = DataManager.ins.data.energyReward.ToString();
        btnAddEnergy_InGame.transform.gameObject.SetActive(true);
        btnAddEnergy_InGame.transform.position = new Vector3(-300, 1000, 0);
        btnAddEnergy_InGame.gameObject.SetActive(true);
        tw1 = btnAddEnergy_InGame.transform.DOMoveX(1500, 20).SetDelay(DataManager.ins.data.timeAppear_EnergyReward);
        tw2 = btnAddEnergy_InGame.transform.DOMoveY(1300, 2).SetLoops(-1, LoopType.Yoyo).SetDelay(10);
    }

    private void OffVibrate()
    {
        Prefs.VIBRATE = 0;
        ActiveButtonSetting(btnOnVibrate_HomeUI, btnOffVibrate_HomeUI, 0);
        ActiveButtonSetting(btnOnVibrate_InGame, btnOffVibrate_InGame, 0);
    }

    private void OnVibrate()
    {
        Prefs.VIBRATE = 1;
        ActiveButtonSetting(btnOffVibrate_HomeUI, btnOnVibrate_HomeUI, 1);
        ActiveButtonSetting(btnOffVibrate_InGame, btnOnVibrate_InGame, 1);
    }

    private void OffSoundEffect()
    {
        Prefs.SOUND_EFFECT = 0;
        SoundManager.ins.AdjustSoundEffect(true);
        ActiveButtonSetting(btnOnSoundEffect_HomeUI, btnOffSoundEffect_HomeUI, 0);
        ActiveButtonSetting(btnOnSoundEffect_InGame, btnOffSoundEffect_InGame, 0);
    }

    private void OnSoundEffect()
    {
        Prefs.SOUND_EFFECT = 1;
        SoundManager.ins.AdjustSoundEffect(false);
        ActiveButtonSetting(btnOffSoundEffect_HomeUI, btnOnSoundEffect_HomeUI, 1);
        ActiveButtonSetting(btnOffSoundEffect_InGame, btnOnSoundEffect_InGame, 1);
    }

    private void OffBGMusic()
    {
        Prefs.BG_MUSIC = 0;
        SoundManager.ins.AdjustBGSound(true);
        ActiveButtonSetting(btnOnBGMusic_HomeUI, btnOffBGMusic_HomeUI, 0);
        ActiveButtonSetting(btnOnBGMusic_InGame, btnOffBGMusic_InGame, 0);
    }

    private void OnBGMusic()
    {
        Prefs.BG_MUSIC = 1;
        SoundManager.ins.AdjustBGSound(false);
        ActiveButtonSetting(btnOffBGMusic_HomeUI, btnOnBGMusic_HomeUI, 1);
        ActiveButtonSetting(btnOffBGMusic_InGame, btnOnBGMusic_InGame, 1);
    }

    private void AddEnergy_Home()
    {
        Prefs.TIME_RECHARGE = 0;
        Prefs.ENERGY++;
        GameManager.ins.outOfEnergy = false;
        ShowEnergyBar_Home();
    }

    private void ActiveButtonSetting(Button btnOn, Button btnOff, int state)
    {
        btnOff.gameObject.SetActive(false);
        btnOn.gameObject.SetActive(true);

        if (state == 1)
        {
            btnOn.transform.parent.GetComponent<Image>().color = Color.green;
        }
        else
        {
            btnOn.transform.parent.GetComponent<Image>().color = Color.gray;
        }
    }

    public void Selected_Character_Type(Button btn)
    {
        btnSelect_Melee.transform.localScale = new Vector3(1, 1, 1);
        btnSelect_Melee.image.sprite = NotSelected_CharacterType_IMG;
        btnSelect_Obstacle.transform.localScale = new Vector3(1, 1, 1);
        btnSelect_Obstacle.image.sprite = NotSelected_CharacterType_IMG;
        btnSelect_Ranged.transform.localScale = new Vector3(1, 1, 1);
        btnSelect_Ranged.image.sprite = NotSelected_CharacterType_IMG;

        if (btn != null)
        {
            btn.image.sprite = Selected_CharacterType_IMG;
            btn.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.2f);
        }
        else
        {
            GameManager.ins.enableSpawn_Camera = false;
        }
    }

    public void Active(GameObject ui)
    {
        settingHome_UI.SetActive(false);
        settingInGame_UI.SetActive(false);
        gameLose_UI.SetActive(false);
        gameWon_UI.SetActive(false);
        fight_UI.SetActive(false);
        upgrade_UI.SetActive(false);
        gamePlay_UI.SetActive(false);
        eventUI.SetActive(false);
        shopUI.SetActive(false);

        if (ui != null)
        {
            ui.SetActive(true);
        }
    }

    private void OpenTheCloud()
    {
        cloud_L.DOMove(new Vector3(-16, 0, 0), 0.5f).SetDelay(1);
        cloud_R.DOMove(new Vector3(16, 0, 0), 0.5f).SetDelay(1).OnComplete(() =>
        {
            Active(gamePlay_UI);
            GameEvent.ins.isStarted.Value = true;
            Invoke("OpenWaitingUI", 0f);
        });
    }

    private void OpenWaitingUI() => waiting_UI.SetActive(true);

    private void CloseTheCloud()
    {
        cloud_L.DOMove(Vector2.zero, 1f).OnComplete(() =>
        {
            GameManager.ins.LoadMap();
            home_UI.SetActive(false);
            OpenTheCloud();
        });
        cloud_R.DOMove(Vector2.zero, 1f).OnComplete(() =>
        {
            OpenTheCloud();
        });
    }

    private void SwitchTap_Menu(Button btn)
    {
        btnShop_Page.transform.GetChild(1).gameObject.SetActive(false);
        btnShop_Page.GetComponent<Image>().color = Color.white;
        btnUpgrade_Page.transform.GetChild(1).gameObject.SetActive(false);
        btnUpgrade_Page.GetComponent<Image>().color = Color.white;
        btnFight_Page.transform.GetChild(1).gameObject.SetActive(false);
        btnFight_Page.GetComponent<Image>().color = Color.white;
        btnEvent_Page.transform.GetChild(1).gameObject.SetActive(false);
        btnEvent_Page.GetComponent<Image>().color = Color.white;
        btnSetting_Page.transform.GetChild(1).gameObject.SetActive(false);
        btnSetting_Page.GetComponent<Image>().color = Color.white;

        if (btn != null)
        {
            btn.GetComponent<Image>().color = Color.grey;
            btn.transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    private void ShowMapInfo()
    {
        if (idxMap <= 0)
        {
            btnBackMap.gameObject.SetActive(false);
        }
        else
        {
            btnBackMap.gameObject.SetActive(true);
        }

        if (idxMap >= DataManager.ins.data.levelDB.listPhase.Count - 1)
        {
            btnNextMap.gameObject.SetActive(false);
        }
        else
        {
            btnNextMap.gameObject.SetActive(true);
        }
        IconMap.sprite = DataManager.ins.data.levelDB.listPhase[idxMap].icon;
        if (idxMap > Prefs.PHASE)
        {
            IconMap.transform.GetChild(0).gameObject.SetActive(true);
            txtRound_Home.gameObject.SetActive(false);
            progressBar.SetActive(false);
        }
        else
        {
            IconMap.transform.GetChild(0).gameObject.SetActive(false);
            txtRound_Home.gameObject.SetActive(true);
            progressBar.SetActive(true);
        }
        txtPhase.text = (idxMap + 1) + "." + DataManager.ins.data.levelDB.listPhase[idxMap].name;
        ShowRound();

        foreach (var p in listProgress)
        {
            p.gameObject.SetActive(false);
        }
        if (idxMap >= Prefs.PHASE)
        {
            for (int i = 0; i < Prefs.ROUND + 1; i++)
            {
                listProgress[i].gameObject.SetActive(true);
            }
        }
        else
        {
            foreach (var p in listProgress)
            {
                p.gameObject.SetActive(true);
            }
        }

        if (idxMap == Prefs.PHASE)
        {
            btnPlay.gameObject.SetActive(true);
        }
        else
        {
            btnPlay.gameObject.SetActive(false);
        }
    }

    public void SetMax_SliderEnergy_InGame(int maxValue)
    {
        slider_Energy_InGame.maxValue = maxValue;
        slider_Energy_InGame.value = maxValue;
        txtEnergy_InGame.text = slider_Energy_InGame.value + "/" + slider_Energy_InGame.maxValue;
    }

    public void UpdateSliderEnergy_InGame(float value)
    {
        slider_Energy_InGame.value -= value;
        txtEnergy_InGame.text = slider_Energy_InGame.value + "/" + slider_Energy_InGame.maxValue;
    }
}

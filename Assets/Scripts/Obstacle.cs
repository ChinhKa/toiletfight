using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : CameraMan
{
    public override void Update()
    {
         
    }

    public override void TakeDame(float dmg)
    {
        HP -= dmg;
        if (HP <= 0)
        {
            GameManager.ins.listCamera.Remove(transform);
            Destroy(gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager ins;

    [Space]
    [Header("SPAWN:")]
    private CharacterType character_Type;
    private CameraInfo cameraInfo;
    private CameraCharacterDB cameraCharacterDB;
    private int character_Rank;
    private int character_Level;
    public bool enableSpawn_Camera;
    [HideInInspector] public float countDown_WaitingUI;

    [Space]
    [Header("ITEMS:")]
    public Transform prefab_Plane;
    public Transform prefab_Boom;

    [Space]
    [Header("LIST OBJECT SPAWN:")]
    [HideInInspector] public List<Transform> listToilet = new List<Transform>();
    [HideInInspector] public List<Transform> listCamera = new List<Transform>();

    [HideInInspector] public Transform goal;
    [HideInInspector] public Slider health_Goal;
    [HideInInspector] public bool outOfEnergy;
    public int quantity_Toilet;
    public Transform energyDrop_Pre;
    private float countDown_Recharge;
    private float countDown_PlayWaitingSound = 0;
    public Transform map;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        if (Prefs.ENERGY <= 0 && PlayerPrefs.HasKey(GameConstants.ENERGY))
        {
            outOfEnergy = true;
            if (Prefs.TIME_RECHARGE <= 0)
            {
                countDown_Recharge = 5;  
            }
            else
            {
                countDown_Recharge = Prefs.TIME_RECHARGE;
            }
        }        
    }

    public void LoadMap()
    {
        ClearMap();
        GameEvent.ins.ClearEvent();
        map =Instantiate(DataManager.ins.data.levelDB.listPhase[Prefs.PHASE].listRound[Prefs.ROUND].map);
        countDown_WaitingUI = 5;
    }

    public void ClearMap()
    {
        if(map != null)
        {
            Destroy(map.gameObject);
            listCamera.Clear();
            listToilet.Clear();
            quantity_Toilet = 0;
        }
    }

    private void Update()
    {
        if (GameEvent.ins.isStarted.Value)
        {
            if(goal == null)
            {
                goal = GameObject.FindGameObjectWithTag("Goal").transform;
                health_Goal = GameObject.FindGameObjectWithTag("HealthGoal").GetComponent<Slider>();
                SetGeneratorHP();
            }
            if (GameEvent.ins.isReady.Value)
            {
                if (Input.GetMouseButton(0) && enableSpawn_Camera)
                {
                    Spawn_Player();
                    SoundManager.ins.PlaySoundEffect(SoundManager.ins.spawnCameraClip);
                }
            }
            else
            {
                if (countDown_WaitingUI <= 0)
                {
                    SoundManager.ins.PlaySoundEffect(SoundManager.ins.readyClip);
                    GameEvent.ins.isReady.Value = true;
                    UIManager.ins.waiting_UI.SetActive(false);
                }
                else
                {
                    if(countDown_PlayWaitingSound < 0)
                    {
                        SoundManager.ins.PlaySoundEffect(SoundManager.ins.coolDownClip);
                        countDown_PlayWaitingSound = 1;
                    }
                    else
                    {
                        countDown_PlayWaitingSound -= Time.deltaTime;
                    }
                 
                    countDown_WaitingUI -= Time.deltaTime;
                    UIManager.ins.txtCountDown_WaitingUI.text = Mathf.Round(countDown_WaitingUI) + "s";
                }
            }
        }
        else
        {
            if (outOfEnergy)
            {
                countDown_Recharge -= Time.deltaTime;
                Prefs.TIME_RECHARGE = (int)countDown_Recharge;
                int minutes = Mathf.FloorToInt(countDown_Recharge / 60);
                int seconds = Mathf.FloorToInt(countDown_Recharge % 60);
                UIManager.ins.txtEnergy_Home.text = string.Format("{0:00}:{1:00}", minutes, seconds);
                if (countDown_Recharge <= 0)
                {
                    Prefs.ENERGY++;
                    UIManager.ins.ShowEnergyBar_Home();
                    outOfEnergy = false;
                }
            }
        }
    }

    #region player
    public void SetSpawnInfo_Camera(CharacterType type)
    {
        character_Type = type;
        enableSpawn_Camera = true;
    }

    private void Spawn_Player()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePosition2D = new Vector2(mousePosition.x, mousePosition.y);

        RaycastHit2D hit = Physics2D.Raycast(mousePosition2D, Vector2.zero);

        if (hit.collider != null)
        {
            return;
        }

        Spawn_Camera_Processing(character_Type, mousePosition2D);
    }

    public void Spawn_Camera_Processing(CharacterType type,Vector2 pos)
    {
        switch (type)
        {
            case CharacterType.obstacle:
                cameraCharacterDB = DataManager.ins.data.camera_Obstacle_DB;
                character_Rank = Prefs.RANK_SELECTED_OBSTACLE;
                character_Level = Prefs.LEVEL_SELECTED_OBSTACLE;
                break;
            case CharacterType.melee:
                cameraCharacterDB = DataManager.ins.data.camera_Melee_DB;
                character_Rank = Prefs.RANK_SELECTED_MELEE;
                character_Level = Prefs.CURRENT_LEVEL_MELEE;
                break;
            case CharacterType.ranged:
                cameraCharacterDB = DataManager.ins.data.camera_Ranged_DB;
                character_Rank = Prefs.RANK_SELECTED_RANGED;
                character_Level = Prefs.LEVEL_SELECTED_RANGED;
                break;
            default:
                break;
        }
  
        cameraInfo = cameraCharacterDB.listCharacter.Find(c => c.type == type && (int)c.rank == character_Rank);
        LevelDetail level = cameraInfo.listLevel.Find(l => (int)l.level == character_Level);
        if(UIManager.ins.slider_Energy_InGame.value >= level.cost_Buy)
        {
            Transform pre = Instantiate(cameraInfo.prefab, pos, Quaternion.identity);
            pre.GetComponent<Character>().SetInfo(type,level.HP, level.ATK, level.ATKS
                , level.range, level.moveSpeed, cameraInfo.attackSound, (int)LayerM.Player, LayerM.Enemy.ToString(),Tag.Player.ToString());
            listCamera.Add(pre);
            pre.SetParent(map);
            UIManager.ins.UpdateSliderEnergy_InGame(level.cost_Buy);
        }
    }


    public void SpawnHero()
    {
        Info heroInfo = DataManager.ins.data.heroInfo;
        Transform pre = Instantiate(heroInfo.prefab, Vector3.zero, Quaternion.identity);
        pre.GetComponent<Character>().SetInfo(CharacterType.hero,heroInfo.HP, heroInfo.ATK, heroInfo.ATKS
            , heroInfo.range, heroInfo.moveSpeed, heroInfo.attackSound, (int)LayerM.Player, LayerM.Enemy.ToString(), Tag.Player.ToString());
        listCamera.Add(pre);
        pre.SetParent(map);
    }
    #endregion

    public void UsePlaneItem()
    {
        Transform plane = Instantiate(prefab_Plane,new Vector2(0,-15),Quaternion.identity);
        Time.timeScale = 0;
        plane.DOMove(new Vector2(0,8),1).OnComplete(()=> {
            Transform boom = Instantiate(prefab_Boom, plane.position, Quaternion.identity);
            boom.DOMove(new Vector2(0, 0), 1f).OnComplete(() => {
                Time.timeScale = 1;
                if(Prefs.VIBRATE == 1)
                {
                    Handheld.Vibrate();
                }
                Transform boss_temp = null;
                Camera.main.transform.DOShakePosition(0.5f, 10, 20);
                try
                {
                    foreach (var o in listToilet)
                    {
                        if (o.GetComponent<Character>().GetCharacterType() != CharacterType.boss)
                        {
                            Debug.Log("alooooo");
                            o.GetComponent<Character>().PlayAnimation(AnimationName.Die.ToString(),false);
                            o.GetComponent<Character>().enabled = false;
                            Destroy(o.gameObject, 0.5f);
                            quantity_Toilet--;

                            if(quantity_Toilet <= 0)
                            {
                                GameEvent.ins.gameWon.Value = true;
                            }
                        }
                        else
                        {
                            boss_temp = o;
                        }
                        Debug.Log("haha");
                    }
                }
                catch (System.Exception)
                {

                    throw;
                }

                Debug.Log(1);
                listToilet.Clear();
                Debug.Log(2);
                if (boss_temp != null)
                {
                    listToilet.Add(boss_temp);
                }
                Debug.Log(3);
                Destroy(boom.gameObject);
                SoundManager.ins.PlaySoundEffect(SoundManager.ins.explodeClip);
                Debug.Log(4);
            }).SetUpdate(true);
            plane.DOMove(new Vector2(0, 15), 0.5f).OnComplete(()=> {
                plane.gameObject.SetActive(false);
            }).SetUpdate(true);
        }).SetUpdate(true);
    }

    public void TakeDame_Goal(float dame)
    {
        health_Goal.value -= dame;
        if (health_Goal.value <= 0)
        {
            GameEvent.ins.gameLost.Value = true;
        }
    }

    public void Reduce_Toilets(int nbr)
    {
        quantity_Toilet -= nbr;
        if(quantity_Toilet <= 0)
        {
            GameEvent.ins.gameWon.Value = true;
        }
    }

    public void ReceiveRewardsRound()
    {
        MapInfo info = DataManager.ins.data.levelDB.listPhase[Prefs.PHASE].listRound[Prefs.ROUND];
        Prefs.COIN += info.rewardsRound;
    }

    public void ReceiveRewardsMission()
    {
        Prefs.COIN += DataManager.ins.data.listRewardsMission[Prefs.REWARDS_TIME].rewardsMission_Coin;
        Prefs.RUBY += DataManager.ins.data.listRewardsMission[Prefs.REWARDS_TIME].rewardsMission_Ruby;
        if (Prefs.REWARDS_TIME < DataManager.ins.data.listRewardsMission.Count)
        {
            Prefs.REWARDS_TIME++;
        }
    }

    public void SetGeneratorHP()
    {
        health_Goal.maxValue = DataManager.ins.data.generatorHP;
        health_Goal.value = DataManager.ins.data.generatorHP;
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

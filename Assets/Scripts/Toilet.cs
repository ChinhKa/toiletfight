using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Toilet : Character
{
    protected Transform energyDrop;
    public override void Start()
    {
        base.Start();
        energyDrop = Instantiate(GameManager.ins.energyDrop_Pre);
        energyDrop.gameObject.SetActive(false);
        energyDrop.SetParent(transform);
    }

    public override void Move()
    {
        if (target == null)
        {
            DetermineTheTarget(GameManager.ins.listCamera);
            if (Vector2.Distance(transform.position + new Vector3(0, 0.5f, 0), GameManager.ins.goal.position) > range)
            {
                if (transform.position.x >= GameManager.ins.goal.position.x - 2 && transform.position.x <= GameManager.ins.goal.position.x + 2)
                {
                    transform.position = Vector3.MoveTowards(transform.position, GameManager.ins.goal.position, moveSpeed * Time.deltaTime);
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(GameManager.ins.goal.position.x, transform.position.y), moveSpeed * Time.deltaTime);
                }
                if (allowTriggeringAnim_Run)
                {
                    PlayAnimation(AnimationName.Run.ToString(), true);
                    allowTriggeringAnim_Run = false;
                }
            }
            else
            {
                target = GameManager.ins.goal;
                allowTriggeringAnim_Run = true;
            }
        }
        else
        {
            CheckDir();
            if (Vector2.Distance(transform.position, target.position) > range)
            {
                rigidbody.constraints = RigidbodyConstraints2D.None;
                target = null;
                if (allowTriggeringAnim_Run)
                {
                    PlayAnimation(AnimationName.Run.ToString(), true);
                    allowTriggeringAnim_Run = false;
                }
            }
            else
            {
                rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
                Attack();
                allowTriggeringAnim_Run = true;
            }
        }
    }

    public override void CheckDir()
    {
        float xDir;
        if (target.position.x > transform.position.x)
        {
            xDir = -1;
        }
        else
        {
            xDir = 1;
        }
        transform.GetChild(0).localScale = new Vector3(Mathf.Abs(transform.GetChild(0).localScale.x) * xDir, transform.GetChild(0).localScale.y, transform.GetChild(0).localScale.z);
    }

    public override void AttackProcessing()
    {
        if (target.GetComponent<Character>() != null)
        {
            target.GetComponent<Character>().TakeDame(ATK);
        }
        else
        {
            GameManager.ins.TakeDame_Goal(ATK);
        }
    }

    public override void TakeDame(float dmg)
    {
        if (attackSound != null)
        {
            SoundManager.ins.PlaySoundEffect(attackSound);
        }
        energyDrop.position = transform.position;
        energyDrop.gameObject.SetActive(true);
        Vector3 v = transform.position;
        energyDrop.DOMove(new Vector3(v.x, v.y + 1), 0.5f).SetEase(Ease.Flash).OnComplete(() =>
        {
            energyDrop.gameObject.SetActive(false);
        });
        UIManager.ins.UpdateSliderEnergy_InGame(-(DataManager.ins.data.attackEnergyBonusList[Prefs.PHASE]));

        HP -= dmg;

        if (!isDie)
        {
            if (HP <= 0)
            {
                GameManager.ins.Reduce_Toilets(1);
                GameManager.ins.listToilet.Remove(transform);
                PlayAnimation(AnimationName.Die.ToString(), false);
                isDie = true;
                Destroy(gameObject, 0.5f);
                this.enabled = false;
            }
        }
    }
}

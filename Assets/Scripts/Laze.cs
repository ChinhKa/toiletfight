using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laze : MonoBehaviour
{
    [HideInInspector] public float damage;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Tag.Player.ToString()))
        {
            collision.gameObject.GetComponent<Character>().TakeDame(damage);
        }
        if (collision.gameObject.CompareTag(Tag.Goal.ToString()))
        {
            GameManager.ins.TakeDame_Goal(damage);
        }
    }
}

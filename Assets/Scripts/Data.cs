using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "New Data")]
public class Data : ScriptableObject
{
    [Header("CAMERA:")]
    public CameraCharacterDB camera_Obstacle_DB;
    public CameraCharacterDB camera_Melee_DB;
    public CameraCharacterDB camera_Ranged_DB;

    [Space]
    [Header("TOILET:")]
    public ToiletCharacterDB toilet_Melee_DB;
    public ToiletCharacterDB toilet_Ranged_DB;

    [Space]
    [Header("BOSS:")]
    public BossCharacterDB boss_Melee_DB;

    [Space]
    [Header("LEVEL MAP:")]
    public LevelDB levelDB;

    [Space]
    [Header("HERO:")]
    public Info heroInfo;

    [Space]
    [Header("PLANE ENERGY:")]
    public int energyReward;
    public int timeAppear_EnergyReward;

    [Space]
    [Header("GENERATOR:")]
    public int generatorHP;

    [Space]
    [Header("ADS REWARD:")]
    public int adsCoinReward;
    public int adsRubyReward;

    [Space]
    [Header("REWARDS MISSION:")]
    public List<RewardsMission> listRewardsMission = new List<RewardsMission>();

    [Space]
    [Header("REWARD ENERGY ATK E:")]
    public List<int> attackEnergyBonusList = new List<int>();
}

[System.Serializable]
public class RewardsMission
{
    public string roundName;
    public int rewardsMission_Coin;
    public int rewardsMission_Ruby;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraRanged : CameraMan
{
    protected Pooling pooling;

    public override void Start()
    {
        base.Start();
        pooling = GetComponent<Pooling>();
    }

    public override void Move()
    {
        if (GameManager.ins.listToilet.Count > 0)
        {
            if(target != null)
            {
                if (Vector2.Distance(transform.position, target.transform.position) <= range)
                {
                    CheckDir();
                    Attack();
                }
                else
                {
                    DetermineTheTarget(GameManager.ins.listToilet);
                }
            }
            else
            {
                DetermineTheTarget(GameManager.ins.listToilet);
                //CheckDir();
            }
        }
    }

    public override void AttackProcessing()
    {
        Invoke("Shoot", 0.5f);
    }

    private void Shoot()
    {
        if(target != null)
        {
            GameObject pool = pooling.GetPool();
            pool.transform.position = transform.position;
            pool.SetActive(true);
            pool.GetComponent<Bullet>().SetTarget(target.transform, ATK);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConstants
{
    public const string ENERGY = "ENERGY"; 
    public const string TIME_RECHARGE = "TIME_RECHARGE"; 
    public const string PHASE = "PHASE"; 
    public const string ROUND = "ROUND"; 
    public const string COIN = "COIN"; 
    public const string RUBY = "RUBY"; 
    public const string BG_MUSIC = "BG_MUSIC"; 
    public const string SOUND_EFFECT = "SOUND_EFFECT"; 
    public const string VIBRATE = "VIBRATE"; 

    public const string CURRENT_LEVEL_MELEE = "CURRENT_LEVEL_MELEE"; 
    public const string CURRENT_LEVEL_RANGED = "CURRENT_LEVEL_RANGED"; 
    public const string CURRENT_LEVEL_OBSTACLE = "CURRENT_LEVEL_OBSTACLE";

    public const string LEVEL_SELECTED_MELEE = "LEVEL_SELECTED_MELEE";
    public const string LEVEL_SELECTED_RANGED = "LEVEL_SELECTED_RANGED";
    public const string LEVEL_SELECTED_OBSTACLE = "LEVEL_SELECTED_OBSTACLE";

    public const string CURRENT_RANK_MELEE = "CURRENT_RANK_MELEE";
    public const string CURRENT_RANK_RANGED = "CURRENT_RANK_RANGED";
    public const string CURRENT_RANK_OBSTACLE = "CURRENT_RANK_OBSTACLE";

    public const string RANK_SELECTED_MELEE = "RANK_SELECTED_MELEE";
    public const string RANK_SELECTED_RANGED = "RANK_SELECTED_RANGED";
    public const string RANK_SELECTED_OBSTACLE = "RANK_SELECTED_OBSTACLE";

    public const string REWARDS_TIME = "REWARDS_TIME";
    public const string REWARDS_RECEIVED = "REWARDS_RECEIVED";  
}

public enum CharacterType
{
    obstacle = 0, 
    melee = 1,
    ranged = 2,
    boss = 3,
    hero = 4
}

public enum MapID
{
    map1 = 0,
    map2 = 1,
    map3 = 2
}

public enum CharacterRank
{
    rank1 = 1,
    rank2 = 2,
    rank3 = 3
}

public enum Characterlevel
{
    level0 = 0,
    level1 = 1,
    level2 = 2,
    level3 = 3
}

public enum LayerM
{
    Player = 6,
    Enemy = 7
}

public enum Tag
{
    Player,
    Enemy,
    Goal
}

public enum AnimationName
{
    Attack,
    Die,
    Hit,
    Idle,
    Run
}

[System.Serializable]
public class Info : BaseInfo
{
    public CharacterType type;
    public string name;
    public Transform prefab;
    public Sprite icon;
    public AudioClip attackSound;
}

public class BaseInfo
{
    public float HP;
    public float ATK;
    public float ATKS;
    public float range;
    public float moveSpeed;
}
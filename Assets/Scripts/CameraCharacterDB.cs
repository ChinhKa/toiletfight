using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CameraCharacterDB", menuName = "New Camera Character DB")]
public class CameraCharacterDB : ScriptableObject
{
    public List<CameraInfo> listCharacter = new List<CameraInfo>();
}

[System.Serializable]
public class CameraInfo
{
    public CharacterType type;
    public string name;
    public CharacterRank rank; 
    public Transform prefab;
    public Transform prefabUI;
    public Sprite icon;
    public List<LevelDetail> listLevel = new List<LevelDetail>();
    public AudioClip attackSound;
}

[System.Serializable] 
public class LevelDetail : BaseInfo
{
    public Characterlevel level;
    public int cost_Buy;
    public int cost_Upgrade;
}
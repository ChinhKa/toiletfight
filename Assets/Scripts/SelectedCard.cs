using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class SelectedCard : MonoBehaviour
{
    public CharacterType type;
    public TextMeshProUGUI txtLevel;
    public TextMeshProUGUI txtCurrentRank;
    public TextMeshProUGUI txtNextRank;
    [SerializeField] private TextMeshProUGUI txtCost;
    [SerializeField] private Image icon_Coin;
    [SerializeField] private Button btnRemove;
    [SerializeField] private List<GameObject> listProgress = new List<GameObject>();
    public Button btnUpgrade;
    public int upgradeCost;
    public bool isMax;
    public bool empty;
    private Transform cameraSpawn;

    private CameraInfo cameraInfo;
    private LevelDetail levelDetail;

    private void Start()
    {
        btnUpgrade.onClick.AddListener(()=> {
            switch (type)
            {
                case CharacterType.obstacle:
                    Upgrade(GameConstants.CURRENT_LEVEL_OBSTACLE, GameConstants.LEVEL_SELECTED_OBSTACLE, GameConstants.CURRENT_RANK_OBSTACLE, GameConstants.RANK_SELECTED_OBSTACLE);
                    break;
                case CharacterType.melee:
                    Upgrade(GameConstants.CURRENT_LEVEL_MELEE, GameConstants.LEVEL_SELECTED_MELEE, GameConstants.CURRENT_RANK_MELEE, GameConstants.RANK_SELECTED_MELEE);
                    break;
                case CharacterType.ranged:
                    Upgrade(GameConstants.CURRENT_LEVEL_RANGED, GameConstants.LEVEL_SELECTED_RANGED, GameConstants.CURRENT_RANK_RANGED, GameConstants.RANK_SELECTED_RANGED);
                    break;
                default:
                    break;
            }
            UIManager.ins.ActiveUpgradeButtonCard();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.upgradeClip);
        });

        btnRemove.onClick.AddListener(() =>
        {
            transform.gameObject.SetActive(false);
            empty = true;
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.clickButtonClip);
        });
    }

    private void Upgrade(string currentLevelKey, string levelSelectedKey, string currentRankKey, string selectedRankKey) {
        int currentLevel = PlayerPrefs.GetInt(currentLevelKey);
        int rankCurrent = PlayerPrefs.GetInt(currentRankKey);
 
        if (currentLevel >= 3 && rankCurrent < 3)
        {
            PlayerPrefs.SetInt(currentLevelKey, 1);
            PlayerPrefs.SetInt(levelSelectedKey, 1);
            PlayerPrefs.SetInt(currentRankKey, rankCurrent + 1);
            PlayerPrefs.SetInt(selectedRankKey, rankCurrent + 1);
            UIManager.ins.CheckSoldierCards();
            SoundManager.ins.PlaySoundEffect(SoundManager.ins.levelUpClip);
        }
        else
        {
            PlayerPrefs.SetInt(currentLevelKey, currentLevel + 1);
            PlayerPrefs.SetInt(levelSelectedKey, currentLevel + 1);
        }
        Prefs.COIN -= upgradeCost;
        PlayerPrefs.Save();
        SetInfo();
        UIManager.ins.ShowCoin();
    }

    public void SetInfo()
    {
        transform.gameObject.SetActive(true);
        empty = false;
        switch (type)
        {
            case CharacterType.obstacle:
                CardInfoProcessing(DataManager.ins.data.camera_Obstacle_DB,GameConstants.LEVEL_SELECTED_OBSTACLE, GameConstants.RANK_SELECTED_OBSTACLE);
                UIManager.ins.obstacle_Icon.sprite = cameraInfo.icon;
                UIManager.ins.txtObstacleCost.text = levelDetail.cost_Buy.ToString();
                break;
            case CharacterType.melee:
                CardInfoProcessing(DataManager.ins.data.camera_Melee_DB,GameConstants.LEVEL_SELECTED_MELEE, GameConstants.RANK_SELECTED_MELEE);
                UIManager.ins.melee_Icon.sprite = cameraInfo.icon;
                UIManager.ins.txtCameraCost_Melee.text = levelDetail.cost_Buy.ToString();
                break;
            case CharacterType.ranged:
                CardInfoProcessing(DataManager.ins.data.camera_Ranged_DB,GameConstants.LEVEL_SELECTED_RANGED, GameConstants.RANK_SELECTED_RANGED);
                UIManager.ins.ranged_Icon.sprite = cameraInfo.icon;
                UIManager.ins.txtCameraCost_Ranged.text = levelDetail.cost_Buy.ToString();
                break;
            default:
                break;
        }
    }

    private void CardInfoProcessing(CameraCharacterDB characterInfo ,string level, string rank)
    {
        int levelCurrent = PlayerPrefs.GetInt(level);
        int rankCurrent = PlayerPrefs.GetInt(rank);

        cameraInfo = characterInfo.listCharacter.Find(n => (int)n.rank == rankCurrent && n.type == type);
        levelDetail = cameraInfo.listLevel.Find(n => (int)n.level == levelCurrent);
        if (cameraSpawn != null)
        {
            Destroy(cameraSpawn.gameObject);
        }
        cameraSpawn = Instantiate(cameraInfo.prefabUI,transform.position + new Vector3(0,60,0),Quaternion.identity);
        cameraSpawn.SetParent(transform.GetChild(0));

        txtCurrentRank.text = rankCurrent.ToString();
        switch (rankCurrent)
        {
            case 1:
                txtLevel.text = "LV" + (levelCurrent + 1);
                break;
            case 2:
                txtLevel.text = "LV" + (levelCurrent + 4);
                break;
            default:
                txtLevel.text = "LV" + (levelCurrent + 7);
                break;
        }
        upgradeCost = levelDetail.cost_Upgrade;
        txtCost.text = upgradeCost.ToString();
        if (rankCurrent <= 2)
        {
            txtNextRank.text = (rankCurrent + 1).ToString();

        }
        else
        {
            txtNextRank.text = "M";
            if (levelCurrent >= 3)
            {
                btnUpgrade.interactable = false;
                icon_Coin.gameObject.SetActive(false);
                txtCost.text = "MAX";
                isMax = true;
            }
        }
        ProgressActive(levelCurrent);
    }

    private void ProgressActive(int level)
    {
        foreach(var p in listProgress)
        {
            p.SetActive(false);
        }

        for(int i = 0; i < level; i++)
        {
            listProgress[i].SetActive(true);
        }
    }
}

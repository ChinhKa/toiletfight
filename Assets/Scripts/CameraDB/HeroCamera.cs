using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroCamera : CameraMelee
{
    public override void TakeDame(float dmg)
    {
        HP -= dmg;
        if (HP <= 0)
        {
            GameManager.ins.listCamera.Remove(transform);
            PlayAnimation(AnimationName.Die.ToString(), false);
            this.enabled = false;
        }
    }
}

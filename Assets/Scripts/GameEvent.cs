using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class GameEvent : MonoBehaviour
{
    public static GameEvent ins;

    private void Awake()
    {
        ins = this;
    }
    private CompositeDisposable subscriptions = new CompositeDisposable();
    public BoolReactiveProperty isStarted { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty isReady { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty gameWon { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty gameLost { get; set; } = new BoolReactiveProperty(false);
    public bool started;
    public bool ready;
    public bool won;
    public bool lose;
    private void Update()
    {
        started = isStarted.Value;
        ready = isReady.Value;
        won = gameWon.Value;
        lose = gameLost.Value;
    }

    private void OnEnable()
    {
        StartCoroutine(Subscribe());
    }
    private IEnumerator Subscribe()
    {
        yield return new WaitUntil(() => ins != null);

        gameLost.ObserveEveryValueChanged(x => x.Value)
            .Subscribe(value =>
            {
                if (value)
                {
                    UIManager.ins.Active(UIManager.ins.gameLose_UI);
                    SoundManager.ins.PlaySoundWinLose(SoundManager.ins.loseClip);
                }
            })
            .AddTo(subscriptions);

        gameWon.ObserveEveryValueChanged(x => x.Value)
            .Subscribe(value =>
            {
                if (value)
                {
                    GameManager.ins.ReceiveRewardsRound();

                    if (Prefs.PHASE == DataManager.ins.data.levelDB.listPhase.Count - 1 && Prefs.ROUND == DataManager.ins.data.levelDB.listPhase[Prefs.PHASE].listRound.Count - 1)
                    {
                        UIManager.ins.btnNextRound.gameObject.SetActive(false);
                    }
                    
                    UIManager.ins.Active(UIManager.ins.gameWon_UI);
                    SoundManager.ins.PlaySoundWinLose(SoundManager.ins.winClip); 
                }
            })
            .AddTo(subscriptions);
    }
    private void OnDisable()
    {
        subscriptions.Clear();
    }

    private void OnDestroy()
    {
    }

    public void ClearEvent()
    {
        isStarted.Value = false;
        isReady.Value = false;
        gameWon.Value = false;
        gameLost.Value = false;
    }
}

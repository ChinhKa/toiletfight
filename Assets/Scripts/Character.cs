﻿using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    protected CharacterType Type;
    [SerializeField] protected float HP;
    [SerializeField] protected float ATK;

    [Space]
    [Header("SOUND")]
    protected AudioClip attackSound;

    protected float ATKS;
    protected float range;
    protected float moveSpeed;
    protected LayerMask layer_Target;
    protected float countDown_ATKS;
    protected Transform target;
    protected Transform nearestTarget;
    protected Rigidbody2D rigidbody;
    [SerializeField] protected SkeletonAnimation skeletonAnimation;

    protected bool allowTriggeringAnim_Run = true;
    protected bool isDie;

    public virtual void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        if (skeletonAnimation != null)
        {
            PlayAnimation(AnimationName.Idle.ToString(), true);
            skeletonAnimation.AnimationState.Complete += HandleAnimationComplete;
        }
        rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    public virtual void Update()
    {
        if (!isDie) { 
            if (!GameEvent.ins.gameLost.Value && !GameEvent.ins.gameWon.Value)
            {
                Move();
            }
            else
            {
                PlayAnimation(AnimationName.Idle.ToString(), true);
            }        
        }
    }

    public void PlayAnimation(string animationName, bool loop)
    {
        skeletonAnimation.AnimationState.SetAnimation(0, animationName, loop);
    }

    protected bool triggerAttack;
    public virtual void HandleAnimationComplete(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name == AnimationName.Die.ToString())
        {
            Destroy(gameObject);
        }
        else if (trackEntry.Animation.Name == AnimationName.Attack.ToString() || trackEntry.Animation.Name == AnimationName.Hit.ToString() || target == null && tag == Tag.Player.ToString())
        {
            PlayAnimation(AnimationName.Idle.ToString(), true);
        }
    }


    public virtual void Attack()
    {
        //rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        if (countDown_ATKS <= 0)
        {
            if (target != null)
            {
                PlayAnimation(AnimationName.Attack.ToString(), false);
                AttackProcessing();
            }
            if(attackSound != null && tag == "Player")
            {
                SoundManager.ins.PlaySoundEffect(attackSound);
            }
            countDown_ATKS = ATKS;
        }
        else
        {
            countDown_ATKS -= Time.deltaTime;
        }
    }

    public virtual void CheckDir()
    {

    }

    public virtual void AttackProcessing()
    {

    }

    public virtual void Move()
    {

    }

    public virtual void DetermineTheTarget(List<Transform> li)
    {
        if (li.Count > 0)
        {
            nearestTarget = li[0];
            foreach (var e in li)
            {
                if (Vector2.Distance(transform.position, e.position) < Vector2.Distance(transform.position, nearestTarget.position))
                {
                    nearestTarget = e;
                }
            }

            target = nearestTarget;
        }
    }

    public virtual void TakeDame(float dmg)
    {

    }

    public virtual void SetInfo(CharacterType type, float hp, float atk, float atks, float range,
        float moveSpeed, AudioClip attackSound, int layerMine, string layerTarget, string tag)
    {
        this.Type = type;
        this.HP = hp;
        this.ATK = atk;
        this.ATKS = atks;
        this.range = range;
        this.moveSpeed = moveSpeed;
        this.attackSound = attackSound;
        gameObject.layer = layerMine;
        this.layer_Target = LayerMask.GetMask(layerTarget);
        this.tag = tag;
    }

    public CharacterType GetCharacterType() => Type;

    public virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (target != null)
        {
            Gizmos.DrawLine(transform.position, target.position);
        }
    }
}

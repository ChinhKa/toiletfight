using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bullet : MonoBehaviour
{
    public float duration;
    public Transform explodeEff_Pre;
    private Transform explodeEff;

    private void Start()
    {
        if(explodeEff_Pre != null)
        {
            explodeEff = Instantiate(explodeEff_Pre);
            explodeEff.gameObject.SetActive(false);
        }
    }

    public void SetTarget(Transform target, float dame)
    {
        Vector3 lock_Target = target.position;
        Vector3 direction = lock_Target - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = rotation;

        transform.DOMove(lock_Target,duration).SetEase(Ease.Flash).OnComplete(()=> {
            if (transform.position == lock_Target)
            {
                if (explodeEff != null)
                {
                    explodeEff.position = transform.position;
                    explodeEff.gameObject.SetActive(true);
                }
                if(target != null && !GameEvent.ins.gameLost.Value)
                {
                    if (target.GetComponent<Character>() == null)
                    {
                        GameManager.ins.TakeDame_Goal(dame);
                    }
                    else
                    {
                        target.GetComponent<Character>().TakeDame(dame);
                    }
                }
                gameObject.SetActive(false);
            }
        });
    }
}

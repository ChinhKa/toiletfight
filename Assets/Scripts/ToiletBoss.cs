﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToiletBoss : Toilet
{
    protected Pooling pooling;
    protected float countDown_UseSkill;
    protected float skillDamage;
    protected float skillusageTime;
    public GameObject skillEff;
    public Laze laze;
    public AudioClip skillSound;

    public override void Start()
    {
        base.Start();
        countDown_UseSkill = skillusageTime;
        skillEff.SetActive(false);
        pooling = GetComponent<Pooling>();
    }

    public override void Update()
    {
        base.Update();
        if (!GameEvent.ins.gameLost.Value && !GameEvent.ins.gameLost.Value && !isDie)
        {
            countDown_UseSkill -= Time.deltaTime;
        }
    }

    public override void AttackProcessing()
    {
        if(countDown_UseSkill <= 0)
        {
            StartCoroutine(UseSkill());  
        }
        else
        {
            Invoke("Shoot", 0.5f);
        }
    }

    private void Shoot()
    {
        if(target != null)
        {
            GameObject pool = pooling.GetPool();
            pool.transform.position = transform.position;
            pool.SetActive(true);
            pool.GetComponent<Bullet>().SetTarget(target.transform, ATK);
        }
    }

    private IEnumerator UseSkill()
    {
        Vector3 parentPosition = transform.position; // Vị trí của đối tượng cha
        Vector3 targetPosition = target.position; // Vị trí của mục tiêu

        Vector3 direction = targetPosition - parentPosition;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Transform childTransform = skillEff.transform; // Lấy transform của đối tượng con
        childTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, angle));

        skillEff.SetActive(true);
        laze.damage = skillDamage;
        SoundManager.ins.PlaySoundEffect(skillSound);
        yield return new WaitForSeconds(0.2f);
        skillEff.SetActive(false);
        countDown_UseSkill = skillusageTime;
    }

    public void SetSkillInfo(float skillDmg, float timeUse, AudioClip skillS)
    {
        skillusageTime = timeUse;
        skillDamage = skillDmg;
        skillSound = skillS;
    }
}

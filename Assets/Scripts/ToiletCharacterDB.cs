using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ToiletCharacterDB", menuName = "New Toilet Character DB")]
public class ToiletCharacterDB : ScriptableObject
{
    public List<ToiletInfo> listCharacter = new List<ToiletInfo>();
}

[System.Serializable]
public class ToiletInfo : BaseInfo
{
    public MapID mapID;
    public CharacterType type;
    public string name;
    public Sprite icon;
    public AudioClip attackSound;
}
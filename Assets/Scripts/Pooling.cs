using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooling :MonoBehaviour
{
    public int quantity;
    public GameObject prefab;
    private List<GameObject> objectsPool = new List<GameObject>();
    private void Start()
    {
        CreatePool();
    }

    private void CreatePool()
    {
        for (int i = 0; i < quantity; i++)
        {
            Spawn();
        }
    }

    private GameObject Spawn()
    {
        GameObject obj = Instantiate(prefab);
        obj.SetActive(false);
        objectsPool.Add(obj);
        obj.transform.SetParent(GameManager.ins.map);

        return obj;
    }

    public GameObject GetPool()
    {
        foreach(var o in objectsPool)
        {
            if (!o.activeInHierarchy)
            {
                return o;
            }
        }

        return Spawn();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Gate : MonoBehaviour
{
    [SerializeField] private float delayTheNextWave;
    [SerializeField] private float delayAppearanceTime;
    [SerializeField] private Transform caution;
    private ToiletCharacterDB toiletCharacterDB;
    private BossCharacterDB bossCharacterDB;
    private BossInfo bossInfo;
    private ToiletInfo toiletInfo;
    private bool isShowed_Caution;
    private bool enableSpawn = true;
    private int waveIndex = 0;

    [SerializeField] private int offsetX;
    [SerializeField] private int offsetY;

    private bool isboss;

    public Queue<Transform> transforms = new Queue<Transform>();

    private void Start()
    {
        caution.gameObject.SetActive(false);

        while (waveIndex <= DataManager.ins.data.levelDB.listPhase[Prefs.PHASE].listRound[Prefs.ROUND].listWave.Count - 1)
        {
            foreach (var w in DataManager.ins.data.levelDB.listPhase[Prefs.PHASE].listRound[Prefs.ROUND].listWave[waveIndex].toiletListAppears)
            {
                GameManager.ins.quantity_Toilet += w.numbersToilet;
            }
            waveIndex++;
        }
        waveIndex = 0;
    }

    void Update()
    {
        if (GameEvent.ins.isStarted.Value && GameEvent.ins.isReady.Value && !GameEvent.ins.gameLost.Value)
        {
            if (delayAppearanceTime <= 0)
            {
                if (!isShowed_Caution)
                {
                    StartCoroutine(ShowCaution());
                    isShowed_Caution = true;
                }

                if (waveIndex >= DataManager.ins.data.levelDB.listPhase[Prefs.PHASE].listRound[Prefs.ROUND].listWave.Count)
                {
                    enableSpawn = false;
                }
                if (enableSpawn)
                {
                    foreach (var w in DataManager.ins.data.levelDB.listPhase[Prefs.PHASE].listRound[Prefs.ROUND].listWave[waveIndex].toiletListAppears)
                    {
                        for (int i = 0; i < w.numbersToilet; i++)
                        {
                            float randX = Random.Range(transform.position.x - offsetX, transform.position.x + offsetX);
                            float randY = Random.Range(transform.position.y - offsetY, transform.position.y + offsetY);
                            Transform pre = Instantiate(w.pre, new Vector2(randX, randY), Quaternion.identity);
                            pre.SetParent(GameManager.ins.map);
                            switch (w.type)
                            {
                                case CharacterType.melee:
                                    toiletCharacterDB = DataManager.ins.data.toilet_Melee_DB;
                                    isboss = false;
                                    break;
                                case CharacterType.ranged:
                                    toiletCharacterDB = DataManager.ins.data.toilet_Ranged_DB;
                                    isboss = false;
                                    break;
                                default:
                                    bossCharacterDB = DataManager.ins.data.boss_Melee_DB;
                                    isboss = true;
                                    break;
                            }
                            if (isboss)
                            {
                                bossInfo = bossCharacterDB.listCharacter.Find(t => (int)t.mapID == Prefs.PHASE);
                                pre.GetComponent<ToiletBoss>().SetSkillInfo(bossInfo.skillDamage, bossInfo.skillUsageTime, bossInfo.skillSound);
                                pre.GetComponent<Character>().SetInfo(bossInfo.type,bossInfo.HP, bossInfo.ATK, bossInfo.ATKS
                               , bossInfo.range, bossInfo.moveSpeed, bossInfo.attackSound, (int)LayerM.Enemy, LayerM.Player.ToString(), Tag.Enemy.ToString());
                            } 
                            else
                            {
                                toiletInfo = toiletCharacterDB.listCharacter.Find(t => (int)t.mapID == Prefs.PHASE);
                                pre.GetComponent<Character>().SetInfo(toiletInfo.type,toiletInfo.HP, toiletInfo.ATK, toiletInfo.ATKS
                                    , toiletInfo.range, toiletInfo.moveSpeed, toiletInfo.attackSound, (int)LayerM.Enemy, LayerM.Player.ToString(), Tag.Enemy.ToString());
                            }
                         
                            pre.gameObject.SetActive(false);
                            transforms.Enqueue(pre);
                        }
                    }
                }
                StartCoroutine(duyet());
                waveIndex++;
                delayAppearanceTime = delayTheNextWave;
            }
            else
            {
                delayAppearanceTime -= Time.deltaTime;
            }
        }
    }

    private IEnumerator duyet()
    {
        while(transforms.Count != 0)
        {
            Transform pre = transforms.Dequeue();
            pre.gameObject.SetActive(true);
            GameManager.ins.listToilet.Add(pre);
            yield return new WaitForSeconds(0.5f);
        }
    }

    private IEnumerator ShowCaution()
    {
        SoundManager.ins.PlaySoundEffect(SoundManager.ins.zombieGrowlingSound);
        caution.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f).SetLoops(-1, LoopType.Yoyo);
        caution.gameObject.SetActive(true);
        yield return new WaitForSeconds(4f);
        caution.gameObject.SetActive(false);
    }
}

 
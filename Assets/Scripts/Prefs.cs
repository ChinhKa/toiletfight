using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prefs
{
    public static int ENERGY
    {
        get => PlayerPrefs.GetInt(GameConstants.ENERGY);
        set
        {
            PlayerPrefs.SetInt(GameConstants.ENERGY, value);
        }
    }

    public static int PHASE
    {
        get => PlayerPrefs.GetInt(GameConstants.PHASE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.PHASE, value);
        }
    }

    public static int ROUND
    {
        get => PlayerPrefs.GetInt(GameConstants.ROUND);
        set
        {
            PlayerPrefs.SetInt(GameConstants.ROUND, value);
        }
    }

    public static int TIME_RECHARGE
    {
        get => PlayerPrefs.GetInt(GameConstants.TIME_RECHARGE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.TIME_RECHARGE, value);
        }
    }

    public static int COIN
    {
        get => PlayerPrefs.GetInt(GameConstants.COIN);
        set
        {
            PlayerPrefs.SetInt(GameConstants.COIN, value);
        }
    }

    public static int RUBY
    {
        get => PlayerPrefs.GetInt(GameConstants.RUBY);
        set
        {
            PlayerPrefs.SetInt(GameConstants.RUBY, value);
        }
    }

    public static int VIBRATE
    {
        get => PlayerPrefs.GetInt(GameConstants.VIBRATE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.VIBRATE, value);
        }
    }

    public static int CURRENT_LEVEL_MELEE
    {
        get => PlayerPrefs.GetInt(GameConstants.CURRENT_LEVEL_MELEE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.CURRENT_LEVEL_MELEE, value);
        }
    }

    public static int CURRENT_LEVEL_OBSTACLE
    {
        get => PlayerPrefs.GetInt(GameConstants.CURRENT_LEVEL_OBSTACLE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.CURRENT_LEVEL_OBSTACLE, value);
        }
    }

    public static int CURRENT_LEVEL_RANGED
    {
        get => PlayerPrefs.GetInt(GameConstants.CURRENT_LEVEL_RANGED);
        set
        {
            PlayerPrefs.SetInt(GameConstants.CURRENT_LEVEL_RANGED, value);
        }
    }

    public static int LEVEL_SELECTED_MELEE
    {
        get => PlayerPrefs.GetInt(GameConstants.LEVEL_SELECTED_MELEE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.LEVEL_SELECTED_MELEE, value);
        }
    }

    public static int LEVEL_SELECTED_OBSTACLE
    {
        get => PlayerPrefs.GetInt(GameConstants.LEVEL_SELECTED_OBSTACLE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.LEVEL_SELECTED_OBSTACLE, value);
        }
    }

    public static int LEVEL_SELECTED_RANGED
    {
        get => PlayerPrefs.GetInt(GameConstants.LEVEL_SELECTED_RANGED);
        set
        {
            PlayerPrefs.SetInt(GameConstants.LEVEL_SELECTED_RANGED, value);
        }
    }

    public static int CURRENT_RANK_MELEE
    {
        get => PlayerPrefs.GetInt(GameConstants.CURRENT_RANK_MELEE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.CURRENT_RANK_MELEE, value);
        }
    }

    public static int CURRENT_RANK_OBSTACLE
    {
        get => PlayerPrefs.GetInt(GameConstants.CURRENT_RANK_OBSTACLE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.CURRENT_RANK_OBSTACLE, value);
        }
    }

    public static int CURRENT_RANK_RANGED
    {
        get => PlayerPrefs.GetInt(GameConstants.CURRENT_RANK_RANGED);
        set
        {
            PlayerPrefs.SetInt(GameConstants.CURRENT_RANK_RANGED, value);
        }
    }

    public static int REWARDS_TIME
    {
        get => PlayerPrefs.GetInt(GameConstants.REWARDS_TIME);
        set
        {
            PlayerPrefs.SetInt(GameConstants.REWARDS_TIME, value);
        }
    }

    public static int REWARDS_RECEIVED
    {
        get => PlayerPrefs.GetInt(GameConstants.REWARDS_RECEIVED);
        set
        {
            PlayerPrefs.SetInt(GameConstants.REWARDS_RECEIVED, value);
        }
    }

    public static int RANK_SELECTED_OBSTACLE
    {
        get => PlayerPrefs.GetInt(GameConstants.RANK_SELECTED_OBSTACLE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.RANK_SELECTED_OBSTACLE, value);
        }
    }

    public static int RANK_SELECTED_RANGED
    {
        get => PlayerPrefs.GetInt(GameConstants.RANK_SELECTED_RANGED);
        set
        {
            PlayerPrefs.SetInt(GameConstants.RANK_SELECTED_RANGED, value);
        }
    }

    public static int RANK_SELECTED_MELEE
    {
        get => PlayerPrefs.GetInt(GameConstants.RANK_SELECTED_MELEE);
        set
        {
            PlayerPrefs.SetInt(GameConstants.RANK_SELECTED_MELEE, value);
        }
    }

    public static float BG_MUSIC
    {
        get => PlayerPrefs.GetFloat(GameConstants.BG_MUSIC);
        set
        {
            PlayerPrefs.SetFloat(GameConstants.BG_MUSIC, value);
        }
    }

    public static float SOUND_EFFECT
    {
        get => PlayerPrefs.GetFloat(GameConstants.SOUND_EFFECT);
        set
        {
            PlayerPrefs.SetFloat(GameConstants.SOUND_EFFECT, value);
        }
    }
}

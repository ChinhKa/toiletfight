using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager ins;

    [Space]
    [Header("DATA:")]
    public Data data;
    public List<int> rewardsReceivedList = new List<int>();

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        if (!PlayerPrefs.HasKey(GameConstants.PHASE))
        {
            Prefs.PHASE = 0;
        }

        if (!PlayerPrefs.HasKey(GameConstants.ROUND))
        {
            Prefs.ROUND = 0;
        }

        if (!PlayerPrefs.HasKey(GameConstants.ENERGY))
        {
            Prefs.ENERGY = 5;
        }

        if (!PlayerPrefs.HasKey(GameConstants.COIN))
        {
            Prefs.COIN = 0;
        }

        if (!PlayerPrefs.HasKey(GameConstants.RUBY))
        {
            Prefs.RUBY = 0;
        }

        

        if (!PlayerPrefs.HasKey(GameConstants.VIBRATE))
        {
            Prefs.VIBRATE = 1;
        }

        if (!PlayerPrefs.HasKey(GameConstants.CURRENT_LEVEL_MELEE))
        {
            Prefs.CURRENT_LEVEL_MELEE = 0;
            Prefs.LEVEL_SELECTED_MELEE = 0;
        }

        if (!PlayerPrefs.HasKey(GameConstants.CURRENT_LEVEL_OBSTACLE))
        {
            Prefs.CURRENT_LEVEL_OBSTACLE = 0;
            Prefs.LEVEL_SELECTED_OBSTACLE  = 0;
        }

        if (!PlayerPrefs.HasKey(GameConstants.CURRENT_LEVEL_RANGED))
        {
            Prefs.CURRENT_LEVEL_RANGED = 0;
            Prefs.LEVEL_SELECTED_RANGED = 0;
        }

        //
        if (!PlayerPrefs.HasKey(GameConstants.CURRENT_RANK_MELEE))
        {
            Prefs.CURRENT_RANK_MELEE = 1;
            Prefs.RANK_SELECTED_MELEE = 1;
        }

        if (!PlayerPrefs.HasKey(GameConstants.CURRENT_RANK_OBSTACLE))
        {
            Prefs.CURRENT_RANK_OBSTACLE = 1;
            Prefs.RANK_SELECTED_OBSTACLE = 1;
        }

        if (!PlayerPrefs.HasKey(GameConstants.CURRENT_RANK_RANGED))
        {
            Prefs.CURRENT_RANK_RANGED = 1;
            Prefs.RANK_SELECTED_RANGED = 1;
        }

        if (!PlayerPrefs.HasKey(GameConstants.REWARDS_TIME))
        {
            Prefs.REWARDS_TIME = 0;
        }

        if (!PlayerPrefs.HasKey(GameConstants.REWARDS_RECEIVED))
        {
            Prefs.REWARDS_RECEIVED = 0;
        }
    }
}
